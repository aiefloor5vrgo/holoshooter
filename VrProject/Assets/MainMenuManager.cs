﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject settingsMenu;

    private bool isOpen = false;

    public void SettingMenu ()
    {
        settingsMenu.SetActive(!settingsMenu.activeSelf);
    }
}
