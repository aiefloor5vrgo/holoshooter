﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PanelManager : MonoBehaviour {

	public Animator initiallyOpen;

	private int m_OpenParameterId;
	private Animator m_Open;

	const string k_OpenTransitionName = "Open";
	const string k_ClosedStateName = "Closed";

	public void OnEnable()
	{
		m_OpenParameterId = Animator.StringToHash (k_OpenTransitionName);

		if (initiallyOpen == null)
			return;

		OpenPanel(initiallyOpen);
	}

	public void OpenPanel (Animator anim)
	{
		if (m_Open == anim)
			return;

		anim.gameObject.SetActive(true);

		anim.transform.SetAsLastSibling();

		CloseCurrent();

		m_Open = anim;
		m_Open.SetBool(m_OpenParameterId, true);
	}

    public void OpenSettingsPanel (Animator anim)
    {
        if (m_Open == anim)
            return;

        foreach (Transform Window in anim.gameObject.transform.parent)
        {
            if (Window.gameObject != anim.gameObject)
                CloseCurrentSettingsPanel();
        }

        anim.gameObject.SetActive(true);

        anim.transform.SetAsLastSibling();

        m_Open = anim;
        m_Open.SetTrigger("Open");
    }

    public void CloseCurrentSettingsPanel()
    {
        if (m_Open == null)
            return;

        m_Open.SetTrigger("Close");
        StartCoroutine(DisablePanelDeleyed(m_Open));
        m_Open = null;
    }

    public void BackSettingsPanel (Transform panelParent)
    {
        foreach (Transform Window in panelParent)
        {
            CloseCurrentSettingsPanel();
        }
    }

    static GameObject FindFirstEnabledSelectable (GameObject gameObject)
	{
		GameObject go = null;
		var selectables = gameObject.GetComponentsInChildren<Selectable> (true);
		foreach (var selectable in selectables) {
			if (selectable.IsActive () && selectable.IsInteractable ()) {
				go = selectable.gameObject;
				break;
			}
		}
		return go;
	}

	public void CloseCurrent()
	{
		if (m_Open == null)
			return;

		m_Open.SetBool(m_OpenParameterId, false);
		StartCoroutine(DisablePanelDeleyed(m_Open));
		m_Open = null;
	}

	IEnumerator DisablePanelDeleyed(Animator anim)
	{
		bool closedStateReached = false;
		bool wantToClose = true;
		while (!closedStateReached && wantToClose)
		{
			if (!anim.IsInTransition(0))
				closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(k_ClosedStateName);

			wantToClose = !anim.GetBool(m_OpenParameterId);

			yield return new WaitForEndOfFrame();
		}

		if (wantToClose)
			anim.gameObject.SetActive(false);
	}

    #region CloseCurrentPause
    public void CloseCurrentPause()
    {
        if (m_Open == null)
            return;

        m_Open.SetBool(m_OpenParameterId, false);
        StartCoroutine(DisablePanelDeleyedPause(m_Open));
        m_Open = null;
    }

    IEnumerator DisablePanelDeleyedPause(Animator anim)
    {
        bool closedStateReached = false;
        bool wantToClose = true;
        while (!closedStateReached && wantToClose)
        {
            if (!anim.IsInTransition(0))
                closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(k_ClosedStateName);

            wantToClose = !anim.GetBool(m_OpenParameterId);

            yield return new WaitForEndOfFrame();
        }

        if (wantToClose)
        {
            anim.transform.parent.parent.gameObject.SetActive(false);
            GameManager.instance.isPaused = false;
        }
    }
    #endregion
}
