﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu (fileName = "HighscoreTable", menuName = "object/highscore")]
public class highscore : ScriptableObject
{
    public List<int> highscoresList;
}
