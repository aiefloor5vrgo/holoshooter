﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TestingShootingText : MonoBehaviour
{
    public TextMeshProUGUI mText;
    public GameObject enemy;
    
    void Start()
    {
        mText = gameObject.GetComponent<TextMeshProUGUI>();   
    }
    void Update()
    {
        updateText();
    }

    void updateText()
    {
        mText.text = "current enemy hp: " + enemy.GetComponent<Enemy>().currentHealth;
    }
}
