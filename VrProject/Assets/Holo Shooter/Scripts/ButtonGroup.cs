﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonGroup : MonoBehaviour
{
    public Button[] buttons;
    public Color highLightColor;
    public Color normalColor;
    public int[] indexOfButtonColors;
    
    // Start is called before the first frame update
    void Start()
    {
        SetSelection(indexOfButtonColors[Random.Range(0,2)]);
    }

    public void SetSelection(int index)
    {
        int counter = 0;
        foreach (Button b in buttons)
        {
            if (counter == index)
                b.GetComponent<Image>().color = highLightColor;
            else
                b.GetComponent<Image>().color = normalColor;
            counter++;
        }
    }

}
