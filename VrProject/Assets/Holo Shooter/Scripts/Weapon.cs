﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    //singleGun
    public AudioSource singleGunSoundFX;
    [Tooltip("Delay before next shot")]
    public float singlegunDelay;
    public float singlegunDamage;
    public int singlegunAmmoCap;
    public GameObject singlegunFirePoint;
    

    [Space]
    //auto
    public AudioSource autoGunSoundFX;
    [Tooltip("Delay before next shot")]
    public float autogunDelay;
    public float autogunDamage;
    public int autogunAmmoCap;
    public GameObject autogunFirePoint;

    [Space]
    //shotgun
    public AudioSource shotGunSoundFX;
    [Tooltip("Delay before next shot")]
    public float shotgunDelay;
    [Tooltip("damage for each shell (3 shells per shot)")]
    public float shotgunDamage;
    public int shotgunAmmoCap;
    public GameObject shotgunFirePoint;

    [Range(0, 10)]
    public int shotgunSpreadSize = 3;
    [Range(0, 30)]
    public int shotgunFallOffStart = 15;
    [Range(0, 1)]
    public float shotgunDmgFallRate = 0.8f;
}
