﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningEnemies : MonoBehaviour
{
    #region Singleton
    private static SpawningEnemies _instance;
    public static SpawningEnemies Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion

    private int m_maxNumberOfEnemies;
    private GameObject[] m_allEnemyTypes;
    private float m_minSpawnDistance;
    private float m_maxSpawnDistance;
    private Vector3 m_spawnRadiusValues;

   

    public bool isSpawning;
    float timer;
    float frequency;

    private void Start()
    {
        
    }

    void Update()
    {
        if (isSpawning)
        {
            timer += Time.deltaTime;
            if (timer >= frequency)
            {
                //spawn
                spawn();
                timer = 0;
            }
        }
    }
    public void SpawningOn( float startDelay, float spawnRate, int maxNumberOfEnemies, GameObject[] allEnemyTypes,
        float minSpawnDistance, float maxSpawnDistance, Vector3 spawnRadiusValues)
    {
        this.m_maxNumberOfEnemies = maxNumberOfEnemies;
        this.m_allEnemyTypes = allEnemyTypes;
        this.m_minSpawnDistance = minSpawnDistance;
        this.m_maxSpawnDistance = maxSpawnDistance;
        this.m_spawnRadiusValues = spawnRadiusValues;
        this.frequency = spawnRate;
        isSpawning = true;
    }

    public void SpawningOff()
    {
        isSpawning = false;
    }

    void spawn()
    {
        int randEnemy = Random.Range(0, this.m_allEnemyTypes.Length);

        float distance = Random.Range(this.m_minSpawnDistance, this.m_maxSpawnDistance);
        float angle = Random.Range(-Mathf.PI, Mathf.PI);
        float step = 1 * Time.deltaTime;
        Vector3 spawnRadiusValues = this.m_spawnRadiusValues;

        Vector3 spawnPosition = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * distance;
        spawnPosition.x = Mathf.Clamp(spawnPosition.x, -spawnRadiusValues.x, spawnRadiusValues.x);
        spawnPosition.y = this.m_spawnRadiusValues.y;
        spawnPosition.z = Mathf.Clamp(spawnPosition.z, -spawnRadiusValues.z, spawnRadiusValues.z);

        GameObject go = Instantiate(this.m_allEnemyTypes[randEnemy], spawnPosition, gameObject.transform.rotation);
        WaveManager.Instance.RegisterEnemy(go.GetComponent<Enemy>());
        go.transform.LookAt(this.gameObject.transform);

        WaveManager.Instance.increaseSpawnCounter();
        Debug.Log("Spawned Enemy");
    }

    
    
}
