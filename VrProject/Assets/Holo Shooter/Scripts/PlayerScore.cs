﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour
{
    public int score = 0;
    private ScoreManager manager;
    public TMPro.TextMeshProUGUI scoreDisplay;

    private void Awake()
    {
        manager = GetComponent<ScoreManager>();
    }

    public void Update()
    {
        ScoreDisplay();
    }

    public int GetScore()
    {
        return score;
    }

    public void AddScore(int toAdd)
    {
        score += toAdd;
    }

    public void SubmitScore()
    {
        if (score != 0)
        {
            manager.CheckScore(score);
        }
    }
    
    public void ScoreDisplay()
    {
        scoreDisplay.text = "Score: " + score;
    }
}
