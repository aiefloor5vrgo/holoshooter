﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public static WeaponManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
    }

    public enum WeaponType
    {
        Singlegun,
        Autogun,
        Shotgun
    }

    public bool useSounds = true;

    [Header("What gun does the player start with?")]
    public bool StartWithSinglegun = false;
    public bool StartWithAutogun = false;
    public bool StartWithShotgun = false;

    private bool canSingleGunShoot = true;
    private bool canWeShoot = true;
    [SerializeField]
    private bool shootOnCooldown = false;

    [HideInInspector] public bool isHoldingWeapon = false;

    //public float bulletVisualDuration;
    //public int ammoPlaceHolder;

    private int shotgunSpreadSize;
    private int shotgunFallOffStart;
    private float shotgunDmgFallOffRate;

    private GameObject Player;
    [HideInInspector]
    public GameObject CurrentWeaponGO;
    public LineRenderer LineRendererCur;

    GameObject WeaponManagerGO;
    Weapon WeaponScript;

    private int currentWeapon;
    private const float FireRate = 1;
    private float FireRateMultiplier;
    private float currentDamage;

    //public Transform bulletSpawnPos;

    private GameObject currentGameObjectHit;
    //private Enemy enemyScript = null;

    public GameObject SingleGunGO;
    public GameObject AutoGunGO;
    public GameObject ShotGunGO;

    public GameObject weaponHolder;
    public GameObject firePoint;
    public TMPro.TextMeshProUGUI text;
    public OVRRaycaster OVR_Raycaster;

    private AudioSource singleGunSoundFX;
    private AudioSource autoGunSoundFX;
    private AudioSource shotGunSoundFX;

    private float singlegunDelay;
    private float autogunDelay;
    private float shotgunDelay;

    private Ammo ammoScript;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

        WeaponManagerGO = GameObject.Find("WeaponManager");
        WeaponScript = WeaponManagerGO.GetComponent<Weapon>();

        singlegunDelay = gameObject.GetComponent<Weapon>().singlegunDelay;
        autogunDelay = gameObject.GetComponent<Weapon>().autogunDelay;
        shotgunDelay = gameObject.GetComponent<Weapon>().shotgunDelay;

        ammoScript = GetComponent<Ammo>();

        singleGunSoundFX = WeaponScript.singleGunSoundFX;
        autoGunSoundFX = WeaponScript.singleGunSoundFX;
        shotGunSoundFX = WeaponScript.singleGunSoundFX;

        shotgunSpreadSize = WeaponScript.shotgunSpreadSize;
        shotgunFallOffStart = WeaponScript.shotgunFallOffStart;
        shotgunDmgFallOffRate = WeaponScript.shotgunDmgFallRate;

        //SingleGunGO = weaponHolder.transform.GetChild(1).gameObject;
        //AutoGunGO = Player.transform.GetChild(1).gameObject;
        //ShotGunGO = Player.transform.GetChild(2).gameObject;

        if (StartWithSinglegun == true)
        {
            AutoGunGO.SetActive(false);
            ShotGunGO.SetActive(false);
        }

        if (StartWithAutogun == true)
        {
            SingleGunGO.SetActive(false);
            ShotGunGO.SetActive(false);
        }

        if (StartWithShotgun == true)
        {
            SingleGunGO.SetActive(false);
            AutoGunGO.SetActive(false);
        }
       
    }

    void Update()
    {
        if (GameManager.instance.isPaused || isHoldingWeapon == false)
            return;

        //decideActiveWeapon();
        WeaponValues();
        shooting();
        reloading();
        decideIfSingleGunCanShoot();
    }

    void decideActiveWeapon()
    {
        for (int i = 0; i >= 0; i++)
        {
            CurrentWeaponGO = weaponHolder.transform.GetChild(i).gameObject;
            if (CurrentWeaponGO.activeInHierarchy == false)
            {
                continue;
            }
            else if (i == weaponHolder.transform.childCount)
            {
                CurrentWeaponGO = null;
            }
            else
            {
                break;
            }
        }
        WeaponValues();
    }

    void WeaponValues()
    {
        Debug.Log(CurrentWeaponGO);
        if (CurrentWeaponGO == SingleGunGO)
        {
            FireRateMultiplier = WeaponScript.singlegunDelay;
            currentDamage = WeaponScript.singlegunDamage;
            firePoint = WeaponScript.singlegunFirePoint;
        }
        if (CurrentWeaponGO == AutoGunGO)
        {

            FireRateMultiplier = WeaponScript.autogunDelay;
            currentDamage = WeaponScript.autogunDamage;
            firePoint = WeaponScript.autogunFirePoint;
        }

        if (CurrentWeaponGO == ShotGunGO)
        {
            FireRateMultiplier = WeaponScript.shotgunDelay;
            currentDamage = WeaponScript.shotgunDamage;
            firePoint = WeaponScript.shotgunFirePoint;
        }
    }

    void reloading()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
        {
            ammoScript.reload();
        }

        //if (Input.GetButtonDown("R"))
        //{
        //    ammoScript.reload();
        //}
    }

    void shooting()
    {
        areWeAllowedToShoot();
        if (canWeShoot == false)
        {
            return;
        }
        
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            rayCastShot();
        }
        
        
        if (Input.GetMouseButtonDown(0))
        {
            rayCastShot();
        }
    }

    void rayCastShot()
    {
        if (shootOnCooldown == false)
        {
            if (CurrentWeaponGO == SingleGunGO && canSingleGunShoot == true || CurrentWeaponGO == AutoGunGO)
            {
                shootOnCooldown = true;
                RaycastHit hit;

                if (Physics.Raycast(firePoint.transform.position, firePoint.transform.forward, out hit, Mathf.Infinity))
                {
                    if (useSounds == true)
                    {
                        if (CurrentWeaponGO == SingleGunGO)
                        {
                            singleGunSoundFX.Play();
                        }

                        if (CurrentWeaponGO == AutoGunGO)
                        {
                            autoGunSoundFX.Play();
                        }
                    }

                    currentGameObjectHit = hit.transform.gameObject;

                    text.text = "Hit Enemy";

                    Enemy enemyScript = currentGameObjectHit.GetComponent<Enemy>();

                    if (enemyScript != null)
                    {
                        Debug.Log("Current weapon damage " + currentDamage);
                        enemyScript.DealDamage(currentDamage);

                        Debug.DrawLine(Vector3.zero, new Vector3(5, 0, 0), Color.green, 2.5f);
                    }
                }
                canSingleGunShoot = false;
            }

            if (CurrentWeaponGO == ShotGunGO)
            {
                Vector3 topShell = new Vector3(firePoint.transform.position.x, firePoint.transform.position.y + shotgunSpreadSize, shotgunSpreadSize);
                Vector3 leftShell = new Vector3(firePoint.transform.position.x - shotgunSpreadSize, firePoint.transform.position.y - shotgunSpreadSize, shotgunSpreadSize);
                Vector3 rightShell = new Vector3(firePoint.transform.position.x + shotgunSpreadSize, firePoint.transform.position.y - shotgunSpreadSize, shotgunSpreadSize);

                Ray ray1 = Camera.main.ScreenPointToRay(topShell);
                Ray ray2 = Camera.main.ScreenPointToRay(leftShell);
                Ray ray3 = Camera.main.ScreenPointToRay(rightShell);

                RaycastHit hit;


                if (Physics.Raycast(ray1, out hit, 100))
                {
                    if (useSounds == true)
                    {
                        shotGunSoundFX.Play();
                    }
                    shotgunFallOff(hit, shotgunFallOffStart);
                    
                    Enemy enemyScript = currentGameObjectHit.GetComponent<Enemy>();

                    if (enemyScript != null)
                    {
                        Debug.Log("Current weapon damage " + currentDamage);
                        enemyScript.DealDamage(currentDamage);

                        Debug.DrawLine(Vector3.zero, new Vector3(5, 0, 0), Color.green, 2.5f);
                    }

                }

                if (Physics.Raycast(ray2, out hit, 100))
                {

                    currentGameObjectHit = hit.transform.gameObject;
                    Enemy enemyScript = currentGameObjectHit.GetComponent<Enemy>();

                    if (enemyScript != null)
                    {
                        Debug.Log("Current weapon damage " + currentDamage);
                        enemyScript.DealDamage(currentDamage);

                        Debug.DrawLine(Vector3.zero, new Vector3(5, 0, 0), Color.green, 2.5f);
                    }
                }

                if (Physics.Raycast(ray3, out hit, 100))
                {

                    currentGameObjectHit = hit.transform.gameObject;
                    Enemy enemyScript = currentGameObjectHit.GetComponent<Enemy>();

                    if (enemyScript != null)
                    {
                        Debug.Log("Current weapon damage " + currentDamage);
                        enemyScript.DealDamage(currentDamage);

                        Debug.DrawLine(Vector3.zero, new Vector3(5, 0, 0), Color.green, 2.5f);
                    }
                }
            }
        }

        ammoScript.removeAmmo(1);
        if (CurrentWeaponGO == SingleGunGO)
        {
            Invoke("setShootOnCooldown", singlegunDelay);
        }

        if (CurrentWeaponGO == AutoGunGO)
        {
            Invoke("setShootOnCooldown", autogunDelay);
        }

        if (CurrentWeaponGO == ShotGunGO)
        {
            Invoke("setHaveShotToFalse", shotgunDelay);
        }
    }

    void setShootOnCooldown()
    {
       shootOnCooldown = false;
    }

    void shotgunFallOff(RaycastHit hit, float fallOff)
    {
        if (hit.distance > fallOff)
        {
            float finalFallOffDamage = fallOff * shotgunDmgFallOffRate;
            float damageFallOff = hit.distance - finalFallOffDamage;

            currentDamage = damageFallOff;
        }
    }


    void testingSwapWeapon()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            int currentWeaponID = Player.transform.GetSiblingIndex();
            CurrentWeaponGO = Player.transform.GetChild(currentWeaponID += 1).gameObject;
            if (CurrentWeaponGO == null)
            {
                CurrentWeaponGO = Player.transform.GetChild(0).gameObject;
            }
        }
    }
    void areWeAllowedToShoot()
    {
        ammoScript.ammoAvaliable(CurrentWeaponGO);


        if (ammoScript.DoWeHaveAmmo == true)
        {
            canWeShoot = true;
        }
        else
        {
            canWeShoot = false;
        }
    }

    void decideIfSingleGunCanShoot()
    {
        if (OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger))
        {
            canSingleGunShoot = true;
        }
        if (Input.GetMouseButtonDown(0))
        {
            canSingleGunShoot = true;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawRay(firePoint.transform.position, firePoint.transform.forward * 100);
    }
}
