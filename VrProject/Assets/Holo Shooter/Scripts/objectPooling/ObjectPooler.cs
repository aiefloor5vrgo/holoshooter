﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    private const int defaultPoolSize = 10;
 
    #region Attributes


    //expand the pool if all objects are current instationated
    public static bool expandWhenNecessary = true;

    //String is going to be the path/key to the prefab
    //List is going to be all the instantiations of prefabs
    //ObjectPools is going to be storing all the object pools by string key-
    public static Dictionary<string, List<GameObject>> objectPools = new Dictionary<string, List<GameObject>>();

    #endregion
    #region Object Pool Checks 

    //object pool checks are going to notify us if an object pool exits, or if an object is available for reuse
    private static bool PoolExistsForPrefab(string prefabPath)
    {
        return objectPools.ContainsKey(prefabPath);
    }

    private static bool IsAvailableForReuse(GameObject gameObject)
    {
        return !gameObject.activeSelf;
    }

    #endregion

    #region Object Pool Creation

    //To expand an object pool when we are trying to grab more than we currently have in another pool
    private static GameObject ExpandPoolAndGetObject(string prefabPath, List<GameObject> pool)
    {
        if (!expandWhenNecessary)
        {
            return null;
        }

        GameObject prefab = Resources.Load<GameObject>(prefabPath);

        //instatiate the prefab and and it to the pool and then return that instance
        GameObject instance = Object.Instantiate(prefab) as GameObject;

        pool.Add(instance);

        return instance;
    }

    //To create an object pool
    public static List<GameObject> createObjectPool(string prefabPath, int count)
    {
        if (count <= 0)
        {
            count = 1;
        }

        GameObject prefab = Resources.Load<GameObject>(prefabPath);

        List<GameObject> objects = new List<GameObject>();

        for (int i = 0; i < count; i++)
        {
            GameObject instance = Object.Instantiate<GameObject>(prefab);

            objects.Add(instance);
            
            //when objects are created we dont want them actived until wanted
            instance.SetActive(false);
        }

        //adding this objectPool to the dictionary/list with the key and objects we made
        objectPools.Add(prefabPath, objects);

        return objects;
    }


    #endregion

    #region Object Retrieval

    //create and retrieve object from pool
    public static GameObject GetPooledObject(string prefabPath, int poolSize = defaultPoolSize)
    {
        //Check to see if we have a pool
        //if not we will create a pool
        //return the first object from here
        if (!PoolExistsForPrefab(prefabPath))
        {
            return CreateAndRetrieveFromPool(prefabPath, poolSize);
        }

        var pool = objectPools[prefabPath];

        GameObject instance;

        //if instance does not equal to null return instance else call the expand pool and get object function
        return (instance = FindFirstAvailablePooledObject(pool)) != null ?
            instance : ExpandPoolAndGetObject(prefabPath, pool);
    }

    private static GameObject CreateAndRetrieveFromPool(string prefabPath, int poolSize = defaultPoolSize)
    {
        createObjectPool(prefabPath, poolSize);
        return GetPooledObject(prefabPath);     
    }
    //iterate over the pool and find the first inactive game object
    private static GameObject FindFirstAvailablePooledObject(List<GameObject> pool)
    {
        for (int i = 0; i < pool.Count; i++)
        {
            //if the current position in the list is avaliable
            if (IsAvailableForReuse(pool[i]))
            {
                //return the current game object and end the for loop
                return pool[i];
            }
        }

        //once every object has been iterated and non got returned
        //return null to declare that there are none avaliable
        return null;
    }

    #endregion

}
