﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBehaviour : MonoBehaviour
{
    GameObject GameManagerGO;
    GameManager GameManagerScript;

    void Start()
    {
        GameManagerGO = GameObject.Find("GameManager");
        GameManagerScript = GameManagerGO.GetComponent<GameManager>();
    }


    void Update()
    {
        Destory();
        
    }

    private void Destory()
    {
        StartCoroutine(timer());
    }

    IEnumerator timer()
    {
        yield return new WaitForSecondsRealtime(GameManagerScript.despawnCubeTime);
        gameObject.SetActive(false);
    }
}
