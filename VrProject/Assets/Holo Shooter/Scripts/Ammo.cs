﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public bool DoWeHaveAmmo = true;

    GameObject currentWeaponGO;

    private int maxSinglegunAmmo;
    private int maxAutogunAmmo;
    private int maxShotgunAmmo;

    private int currentSinglegunAmmo;
    private int currentAutogunAmmo;
    private int currentShotgunAmmo;

   
    [Tooltip("Seconds")]
    public float timeToReload = 3;
    void Start()
    {
        maxSinglegunAmmo = gameObject.GetComponent<Weapon>().singlegunAmmoCap;
        maxAutogunAmmo = gameObject.GetComponent<Weapon>().autogunAmmoCap;
        maxShotgunAmmo = gameObject.GetComponent<Weapon>().shotgunAmmoCap;

        currentSinglegunAmmo = maxSinglegunAmmo;
        currentAutogunAmmo = maxAutogunAmmo;
        currentShotgunAmmo = maxShotgunAmmo;
    }

    private void Update()
    {
        currentWeaponGO = gameObject.GetComponent<WeaponManager>().CurrentWeaponGO;
    }

    public void removeAmmo(int amount)
    {
        if (currentWeaponGO.tag == "Singlegun")
        {
            if (currentSinglegunAmmo > 1)
            {
                currentSinglegunAmmo -= amount;
            }
            //auto reload
            else if (currentSinglegunAmmo == 1)
            {
                currentSinglegunAmmo -= amount;
                Invoke("reload", timeToReload);     
            }
        }

        if (currentWeaponGO.tag == "Autogun")
        {
            if (currentAutogunAmmo > 1)
            {
                currentAutogunAmmo -= amount;
            }
            //auto reload
            else if (currentAutogunAmmo == 1)
            {
                currentAutogunAmmo -= amount;
                Invoke("reload", timeToReload);
            }
        }

        if (currentWeaponGO.tag == "Shotgun")
        {
            if (currentShotgunAmmo > 1)
            {
                currentShotgunAmmo -= amount;
            }
            //auto reload
            else if (currentShotgunAmmo == 1)
            {
                currentShotgunAmmo -= amount;
                Invoke("reload", timeToReload);
            }
        }
    }

    public void ammoAvaliable(GameObject currentWeaponGO)
    {
        if (currentWeaponGO.tag == "Singlegun")
        {
            if (currentSinglegunAmmo > 0)
            {
                DoWeHaveAmmo = true;
            }
            else
            {
                DoWeHaveAmmo = false;
            }
        }

        if (currentWeaponGO.tag == "Autogun")
        {
            if (currentAutogunAmmo > 0)
            {
                DoWeHaveAmmo = true;
            }
            else
            {
                DoWeHaveAmmo = false;
            }
        }

        if (currentWeaponGO.tag == "Shotgun")
        {
            if (currentShotgunAmmo > 0)
            {
                DoWeHaveAmmo = true;
            }
            else
            {
                DoWeHaveAmmo = false;
            }
        }
    }

    public void reload()
    {
        if (currentWeaponGO.tag == "Singlegun")
        {
            currentSinglegunAmmo = maxSinglegunAmmo;
        }

        if (currentWeaponGO.tag == "Autogun")
        {
            currentAutogunAmmo = maxAutogunAmmo;
        }

        if (currentWeaponGO.tag == "Shotgun")
        {
            currentShotgunAmmo = maxShotgunAmmo;
        }
    }
}
