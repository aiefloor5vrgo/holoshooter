﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    //This class can be merged with any other manager if only one is present in the scene at any time.


    private void Update()
    {
        OVRInput.Update();
    }

    private void FixedUpdate()
    {
        OVRInput.FixedUpdate();
    }
}
