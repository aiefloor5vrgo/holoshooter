﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Health : MonoBehaviour
{
    public int allowedHits;

    public float timer;

    private float currentTimer;

    public int currentHits;

    private bool counting;

    public PlayerScore playerScore;

    public Text debugText;

    private void Awake()
    {
        currentTimer = 0f;
        currentHits = 0;

        if (timer <= 0f)
        {
            Debug.LogError("Hit timer is currently 0 or less, this will lead to instant game over");
        }
    }

    private void Update()
    {
        debugText.text = "Current Hits " + currentHits + " " + "Allowed Hits " + allowedHits;

        if (currentHits >= allowedHits)
        {
            KillPlayer();
        }

        if (counting == true)
        {
            currentTimer += Time.deltaTime;
        }

        if (currentTimer >= timer)
        {
            counting = false;
            currentTimer = 0f;
        }
    }

    private void LateUpdate()
    {
        if (currentHits >= allowedHits)
        {
            KillPlayer();
        }
    }

    public void KillPlayer ()
    {
        playerScore.SubmitScore();
        SceneManager.LoadScene("Main Menu");
    }

    public void StartCounting()
    {
        counting = true;
    }

    public void Hit()
    {
        currentHits += 1;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.GetComponent<Enemy>() || other.tag == "Enemies")
        {
            currentHits += 1;
        }
    }
}
