﻿using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    public UnityEvent onTrigger;

    public void Interact()
    {
        onTrigger.Invoke();
    }
}
