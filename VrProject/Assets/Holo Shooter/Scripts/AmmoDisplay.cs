﻿using UnityEngine;
using UnityEngine.UI;

public class AmmoDisplay : MonoBehaviour
{
    [Tooltip("Toggle if the display will show what a full magazine is (14/18 instead of 14)")]
    public bool showOutOf = true;

    [Tooltip("The character that seperates the current and maximum ammo counts.")]
    public char seperatingIcon = '/';

    //Insert whatever script manages the ammo
    // public Ammo ammoManager; (you can uncomment this when you have an ammo script)

    private Text ammoDisplay;

    private void Awake()
    {
        ammoDisplay = GetComponent<Text>();
    }

    private void Update()
    {
        if (showOutOf) ;
        // ammoDisplay.text = ammoManager.GetCurrentAmmo().ToString() /* Replace this with however to get the current ammo count */ + seperatingIcon + ammoManager.GetMaxAmmo().ToString() /*replace this with whatever gets the maximum capacity*/;
        else;
           // ammoDisplay.text = ammoManager.GetCurrentAmmo().ToString() /* Replace this with however to get the current ammo count */;
    }
}

// just commented some stuff out to stop errors. uncomment anything you need to when you have everything you need - Jay
