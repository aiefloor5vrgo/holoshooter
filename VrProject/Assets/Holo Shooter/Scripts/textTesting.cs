﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class textTesting : MonoBehaviour
{
    public TextMeshProUGUI text;
    public Enemy currentEnemy;

    void Start()
    {
        
    }

    void Update()
    {
        updateText();
    }

    void updateText()
    {
        text.text = "enemy HP: " + currentEnemy.currentHealth.ToString();
    }
}
