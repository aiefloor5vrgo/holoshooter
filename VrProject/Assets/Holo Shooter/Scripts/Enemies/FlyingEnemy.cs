﻿using UnityEngine;

public class FlyingEnemy : Enemy
{
    AudioSource soundSource;
    public AudioClip movement;
    public AudioClip explode;
    private void Start()
    {
        soundSource = GetComponent<AudioSource>();
        soundSource.clip = movement;
    }
    void Update()
    {
        if(health <= 0)
        {
            Die();
            soundSource.clip = explode;
        }
        transform.LookAt(target);
        transform.Translate(new Vector3(0, 0, moveSpeed) * Time.deltaTime, Space.Self);
        if (Vector3.Distance(transform.position, target) < 0.1f)
        {
            soundSource.clip = explode;
            Destroy(gameObject);
            
        }
    }
}
