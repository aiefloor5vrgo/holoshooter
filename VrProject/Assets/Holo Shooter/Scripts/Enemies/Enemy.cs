﻿using UnityEngine;
using UnityEngine.UI;

public abstract class Enemy : MonoBehaviour
{
    public float health;
    public int scoreValue;
    public float moveSpeed;

    public PlayerScore scoreHandler;
    protected Vector3 target;

    public float currentHealth;

    protected Health healthScript;


    protected bool isVisible;


    protected virtual void Awake()
    {
        currentHealth = health;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform.position;
        healthScript = player.GetComponentInChildren<Health>();
        isVisible = false;
    }

    private void OnBecameInvisible()
    {
        isVisible = false;
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    public virtual void Kill()
    {
        /*
        Insert whatever code for explosions here.
        */
        GameManager.instance.listOfEnemies.Remove(gameObject);
    }

    protected void Die()
    {
        scoreHandler.AddScore(scoreValue);

        GameManager.instance.listOfEnemies.Remove(gameObject);
        Destroy(gameObject);

        Debug.Log("Death");
    }

    public void DealDamage(float damage)
    {

        currentHealth -= damage;
        if (currentHealth <= 0f)
        {
            Die();

        }
    }

    public void DealDamage(float damage, Text debugText)
    {

        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            debugText.text = scoreHandler.score.ToString();
            scoreHandler.AddScore(1);
        }
    }

}
