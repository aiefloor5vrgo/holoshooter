﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class GroundEnemy : Enemy
{

    private NavMeshAgent navAgent;

    private void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.destination = target;
        navAgent.speed = moveSpeed;
    }

    private void Update()
    {
        if (health <= 0)
        {
            Die();
        }
    }
}
