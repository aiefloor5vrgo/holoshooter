﻿using UnityEngine;

public class FlyingShootingEnemy : Enemy
{

    public float strafeDistance;
    public float shootDelay;
    private bool arrived;
    private bool invoking;
    public AudioClip movement;
    public AudioClip shootfx;
    public AudioClip explode;
    AudioSource soundSource;

    private void Start()
    {
        arrived = false;
        if (strafeDistance == 0f)
            strafeDistance = 5f;
        if (shootDelay == 0f)
            shootDelay = 1f;
        soundSource = GetComponent<AudioSource>();
        soundSource.clip = movement;
    }

    private void Update()
    {
        if (health <= 0)
        {
            soundSource.clip = explode;
            Die();
        }
        transform.LookAt(target);
        if (!arrived)
        {
            if (Vector3.Distance(target, transform.position) <= strafeDistance)
                arrived = true;
            transform.Translate(new Vector3(0, 0, moveSpeed) * Time.deltaTime, Space.Self);
        }
        else
        {
            if (!invoking)
            {
                InvokeRepeating("Fire", 0f, shootDelay);
                invoking = true;
            }

            transform.Translate(new Vector3(moveSpeed, 0, 0) * Time.deltaTime, Space.Self);
        }

    }

    private void Fire()
    {
            healthScript.Hit();
        soundSource.clip = shootfx;
        soundSource.Play();
}
}
