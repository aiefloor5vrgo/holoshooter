﻿using UnityEngine;

public class GroundShootingEnemy : Enemy
{
    public float shootDelay = 1f;

    public float spinDuration = 2f;

    public float waitDelay = 0.5f;

    public float spinSpeed = 5f;

    public float hitSpeed = 4f;

    [Range(0, 1)]
    public float lookAtSpeed = 0.5f;

    private bool spinning;
    private Quaternion lookDir;
    private float ticker = 0f;
    private bool fire;
    public AudioClip shootfx;
    public AudioClip explode;
    AudioSource soundSource;


    private void Start()
    {
        spinning = true;
        fire = false;
        lookDir = Quaternion.LookRotation(target - transform.position, Vector3.up);
        InvokeRepeating("Shoot", 2, hitSpeed);

        soundSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (health <= 0)
        {
            soundSource.clip = explode;
            Die();
        }
        if (spinning)
        {
            transform.Rotate(new Vector3(0, spinSpeed, 0));
            ticker += Time.deltaTime;
            if (ticker >= spinDuration)
            {
                spinning = false;
                ticker = 0f;
            }
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, lookDir, lookAtSpeed);
            if(transform.rotation == lookDir)
            {
                fire = true;
            }
        }
        if(fire)
        {
            ToggleSpin();
            Invoke("Shoot", shootDelay);
            Invoke("ToggleSpin", shootDelay + waitDelay);
            fire = false;
        }
    }

    private void ToggleSpin()
    {
        spinning = !spinning;
        
    }

    private void Shoot()
    {
        soundSource.clip = shootfx;
        healthScript.Hit();
    }
}
