﻿using UnityEngine;

public class RayInteract : MonoBehaviour
{
    public KeyCode interactKey = KeyCode.E;
    public OVRInput.Button interactVRKey = OVRInput.Button.PrimaryIndexTrigger;
    public float interactDistance = 10f;

    private void Update()
    {
#if UNITY_EDITOR
        if(Input.GetKey(interactKey))
        {
#elif UNITY_ANDROID
        if(OVRInput.Get(interactVRKey))
        {
#endif
            RaycastHit hit;
            if(Physics.Raycast(transform.position, transform.forward, out hit, interactDistance))
            {
                Interactable interactableObject = hit.transform.GetComponent<Interactable>();
                if(interactableObject)
                {
                    interactableObject.Interact();
                }
            }
        }
    }

}
