﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    static string accessName = "HighScore";

    public int numOfScores = 10;

    public Text highscoretext;

    public highscore highScores;


    private void Start()
    {
        //Create new list
        //highScores.highscoresList = new  List<int>();
        //Fill it with the saved high scores
        LoadScores();

        //if (highScores.highscoresList.Count != numOfScores)
        //    Debug.LogError("Potentially fatal error. Highscore list is not the same size as was input.");

        //A modified sort to put the highest result at the top instead of at the bottom. (Using a lambda expression)
        SortScores();

        if (highScores.highscoresList[0] == 0)
            Debug.Log("All scores empty.");


    }
    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "Main Menu")
        {
            highscoretext.text = highScores.highscoresList[0].ToString();
        }
    }

    /// <summary>
    /// Check if number is a new high score. Does not save.
    /// </summary>
    /// <param name="score">Score to test.</param>
    /// <returns>Returns true if the passed in number is a new high score.</returns>
    public bool CheckScore(int score)
    {
        int highscoreSize = highScores.highscoresList.Count - 1;

        if (score > highScores.highscoresList[highscoreSize])
        {
            highScores.highscoresList.RemoveAt(highscoreSize);
            highScores.highscoresList.Add(score);
            SortScores();
            return true;
        }
        else
            return false;
    }

    #region Utility Functions

    /// <summary>
    /// Add a value to the saved data directly. Note that you will need to load the scores again to display them in-game.
    /// </summary>
    /// <param name="index">The highscore number to be modified.</param>
    /// <param name="value">The modified value of the high score.</param>
    public void AddEntry(int index, int value)
    {
        string accessName = "HighScore" + index;
        PlayerPrefs.SetInt(accessName, index);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Manually get a score from the saved data.
    /// </summary>
    /// <param name="index">The highscore number to access.</param>
    /// <returns>Returns the score. 0 means no data exists.</returns>
    public int GetEntryFromData(int index)
    {
        string accessName = "HighScore" + index;
        return PlayerPrefs.GetInt(accessName);
    }

    /// <summary>
    /// Manually get a score from the currently loaded data.
    /// </summary>
    /// <param name="index">The highscore number to access.</param>
    /// <returns>Returns the score. -1 means out of range. 0 means no data exists.</returns>
    public int GetEntryFromList(int index)
    {
        if (index > highScores.highscoresList.Count)
            return -1;
        return highScores.highscoresList[index];
    }

    /// <summary>
    /// Manually load the scores from the database again.
    /// </summary>
    public void LoadScores()
    {
        for (int i = 0; i < numOfScores; i++)
        {
            //Each high score is stored as HighScore(number)
            string accessName = "HighScore" + i;
            highScores.highscoresList.Add(PlayerPrefs.GetInt(accessName));
        }
    }

    /// <summary>
    /// Utility function to sort the highscores if outside tampering is done with them.
    /// </summary>
    public void SortScores()
    {
        highScores.highscoresList.Sort((a, b) => b.CompareTo(a));
    }

    #endregion

    /// <summary>
    /// Returns a list of ints of the high scores.
    /// </summary>
    /// <returns></returns>
    public List<int> GetScores()
    {
        return highScores.highscoresList;
    }

    #region Delete Functions

    /// <summary>
    /// Delete all saved scores. This will not delete currently loaded scores.
    /// </summary>
    public void ClearSavedScores()
    {
        PlayerPrefs.DeleteAll();
    }

    /// <summary>
    /// Delete the currently loaded scores. This will not delete saved scores.
    /// </summary>
    public void ClearCurrentScores()
    {
        highScores.highscoresList.Clear();
    }

    #endregion

    /// <summary>
    /// Manually save the highscores. Note that this does not sort them.
    /// </summary>
    public void SavePrefs()
    { 
        for (int i = 0; i < numOfScores; i++)
        {
            PlayerPrefs.SetInt(accessName + i, highScores.highscoresList[i]);
        }
        PlayerPrefs.Save();
    }

    private void OnApplicationQuit()
    {
        SavePrefs();
    }
}