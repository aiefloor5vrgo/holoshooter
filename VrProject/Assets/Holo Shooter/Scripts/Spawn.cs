﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{ 

    private const string CUBE_PREFAB_PATH = "Prefabs/Cube";
    private const string BULLET_PREFAB_PATH = "Prefabs/Bullet";

    void Start()
    {

    }

    void Update()
    {
       leftClickSpawn();
    }

    void leftClickSpawn()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            spawnBullet(Vector3.zero, new Quaternion(0, 0, 0, 0));
        }
    }

    void spawnBullet(Vector3 position, Quaternion rotation)
    {
        var bullet = ObjectPooler.GetPooledObject(CUBE_PREFAB_PATH);

        bullet.transform.position = position;
        bullet.transform.rotation = rotation;
        
        bullet.SetActive(true);
    }

    void spawnCubeIn(Vector3 position, Quaternion rotation)
    {
        var cube = ObjectPooler.GetPooledObject(CUBE_PREFAB_PATH);

        cube.transform.position = position;
        cube.transform.rotation = rotation;

        cube.SetActive(true);
    }
}
