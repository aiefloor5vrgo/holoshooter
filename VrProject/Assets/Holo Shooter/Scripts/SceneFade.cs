﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneFade : MonoBehaviour
{
    public Animator animator;
    
    private int levelToLoad;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //FadeToNextLevel();
            FadeToLevel();
        }
    }
    public void FadeToNextLevel()
    {
        //FadeToLevel(SceneManager.GetActiveScene().buildIndex + 1);

    }

    public void FadeToLevel (/*int levelIndex*/)
    {
        //levelToLoad = levelIndex;
        animator.SetTrigger("fadeOut");
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelToLoad);
    }
}
