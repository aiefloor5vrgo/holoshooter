﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    #region Singleton
    private static WaveManager _instance;
    public static WaveManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion

    public enum SpawnState
    {
        SPAWNING,
        WAITING,
        COUNTING
    }


    [System.Serializable]

    public class Wave
    {
        public Transform enemy;
        public int maxEnemies;
    }

    public PlayerScore playerScore;

    public GameObject[] AllEnemyTypes;
    public float SpawnRandiusSize = 20;
    private Vector3 spawnRadiusValues;



    private bool firstSpawn = true;

    public float minSpawnDistance = 20;
    public float maxSpawnDistance = 20;
    public float timeBetweenSpawns;
    public float timeBetweenWaves;

    public int spawnCounter = 0;

    int currentDiffIncrease;
    bool increasingDiff = false;

    [Space]
    [Range(0.01f, 1f)]
    [Tooltip("Damage works like firerate so the higher this number is more time gets taken off the firerate")]
    public float DamageIncrease = 1;
    public int spawnAmountIncrease = 5;

    [Space]

    public Wave wave;
    private int nextWave = 0;

    private float searchCountdown = 1f;
    public SpawnState state = SpawnState.WAITING;
    public ParticleSystem ParticleSystem;
    public bool inMenu;


    public List<Enemy> enemies = new List<Enemy>();

    SpawningEnemies SpawningEnemiesScript;

    void Start()
    {
        spawnRadiusValues = new Vector3(SpawnRandiusSize, 0, SpawnRandiusSize);
    }
    private void LateUpdate()
    {
        // remove all null items
        enemies.RemoveAll(item => item == null);
    }

    void Update()
    {

        if (state == SpawnState.WAITING)
        {
            // check if enemies are still alive
            if (firstSpawn != true)
            {
                randomIncrease();
            }
            if (EnemiesAreAlive() != true)
            {
                WaveCompleted();
                firstSpawn = false;
            }

        }

        if (state == SpawnState.SPAWNING && spawnCounter < wave.maxEnemies)
        {
            increasingDiff = false;
            Debug.Log("Spawning Enemy from update");
            SpawningEnemies.Instance.SpawningOn(2.0f, 1.0f, wave.maxEnemies, AllEnemyTypes, minSpawnDistance, maxSpawnDistance, spawnRadiusValues);
        }

        if (state == SpawnState.SPAWNING && spawnCounter >= wave.maxEnemies)
        {
            increasingDiff = false;
            SpawningEnemies.Instance.SpawningOff();
            Debug.Log("SpawningOFF");
            state = SpawnState.WAITING;
        }

        //killAllEnemies();
    }

    public void increaseSpawnCounter()
    {
        spawnCounter++;
    }
    // checking if wave is complete
    void WaveCompleted()
    {
        Debug.Log("Wave Completed");
        spawnCounter = 0;
        state = SpawnState.COUNTING;

        state = SpawnState.SPAWNING;

    }

    // seeing if the emenies are still alive
    bool EnemiesAreAlive()
    {
        if (enemies.Count != 0)
        {
            return true;
        }
        else
        {
            if (firstSpawn != true)
            {
                randomIncrease();

            }
            else if (firstSpawn == true)
            {
                firstSpawn = false;
            }

            Debug.Log("No EnemiesAlive");
            return false;
        }
    }

    void randomIncrease()
    {
        if (increasingDiff == false)
        {
            increasingDiff = true;
            currentDiffIncrease = Random.Range(1, 2);

            //AMOUNT SPAWNED INCREASE
            if (currentDiffIncrease == 1)
            {
                Debug.Log("Spawn amount increased");
                wave.maxEnemies += spawnAmountIncrease;
            }

            //DAMAGE INCREASE
            if (currentDiffIncrease == 2)
            {
                Debug.Log("Damage increased");
                foreach (Enemy enemyGO in enemies)
                {
                    if (enemyGO.GetComponent<GroundShootingEnemy>() != null)
                    {
                        enemyGO.GetComponent<GroundShootingEnemy>().shootDelay -= DamageIncrease;
                    }

                    if (enemyGO.GetComponent<FlyingShootingEnemy>() != null)
                    {
                        enemyGO.GetComponent<FlyingShootingEnemy>().shootDelay -= DamageIncrease;
                    }
                }
            }
        }
    }

    public void RegisterEnemy(Enemy enemy)
    {
        this.enemies.Add(enemy);
        enemy.scoreHandler = playerScore;
        GameManager.instance.listOfEnemies.Add(enemy.gameObject);
    }

    public void RemoveEnemy(Enemy enemy)
    {
        this.enemies.Remove(enemy);
        GameManager.instance.listOfEnemies.Remove(enemy.gameObject);
    }

}
