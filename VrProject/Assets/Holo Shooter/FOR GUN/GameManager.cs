﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }

        instance = this;
    }

    [Tooltip("Seconds")]
    [Range(1, 20)]
    public int despawnCubeTime;
    public GameObject playerGameobject;

    public TMPro.TextMeshProUGUI warningText;
    public Image leftBar;
    public Image rightBar;
    public Image bottomBar;
    public Image topBar;
    public AnimationCurve curve;

    [Header("-- UI --")]
    public GameObject PauseHolder;
    public GameObject MainUIHolder;
    [Space]
    public PanelManager menuManager;
    [Space]
    public GameObject weaponInHand;
    [Space]
    public List<GameObject> listOfEnemies;

    [HideInInspector] public bool isPaused = false;
    [HideInInspector] public bool isHoldingWeapon = false;

    private void Start()
    {
        isHoldingWeapon = false;
    }

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Back) || Input.GetKeyDown(KeyCode.Space))
        {
            if (isPaused)
                UnPause();
            else
                PauseGame();
        }
    }

    private void LateUpdate()
    {
        // remove all null items
        listOfEnemies.RemoveAll(item => item == null);

        if (listOfEnemies.Count != 0)
            listOfEnemies.Sort(SortByDistanceToMe);
    }

    int SortByDistanceToMe(GameObject a, GameObject b)
    {
        float squaredRangeA = (a.transform.position - transform.position).sqrMagnitude;
        float squaredRangeB = (b.transform.position - transform.position).sqrMagnitude;
        return squaredRangeA.CompareTo(squaredRangeB);
    }

    private void PauseGame()
    {
        isPaused = true;
        Time.timeScale = 0;
        AudioListener.volume = 0;

        PauseHolder.SetActive(isPaused);
        MainUIHolder.SetActive(!isPaused);
    }

    public void UnPause()
    {
        isPaused = false;
        Time.timeScale = 1;

        AudioListener.volume = 1;

        menuManager.CloseCurrentPause();

        MainUIHolder.SetActive(!isPaused);
    }
}
