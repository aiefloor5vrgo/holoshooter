﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRenderer : MonoBehaviour
{
    private LineRenderer lr;

    public LineRenderer pointerLr;
    public GameObject pointer;
    [Space]
    public GameObject selectedWeapon;
    public GameObject weaponInHand;

    public bool isHoldingWeapon = false;

    void Start()
    {
        lr = pointerLr;
        lr.material = new Material(Shader.Find("Sprites/Default"));

        LaserBegin();
    }

    private void Update()
    {
        if(GetDirection(OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad)) == Vector2.down && OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
        {
            PutAwayWeapon(weaponInHand);
        }
    }

    public Vector2 GetDirection(Vector2 input)
    {
        Vector2[] directions = new Vector2[] 
        {
        Vector2.up,
        Vector2.right,
        Vector2.down,
        Vector2.left
        };

        Vector2 direction = Vector2.zero;
        float max = Mathf.NegativeInfinity;

        foreach (Vector2 vec2 in directions)
        {
            float dot = Vector2.Dot(vec2, input.normalized);

            if (dot > max)
            {
                direction = vec2;
                max = dot;
            }
        }

        return direction;
    }

    private void FixedUpdate()
    {
        if (isHoldingWeapon)
            return;

        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, lr.GetPosition(1).z))
        {
            if (hit.transform.CompareTag("Grabable"))
            {
                SetLaserColor(Color.blue, Color.cyan);
                if(OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown(KeyCode.Escape))
                {
                    weaponInHand = hit.transform.GetComponentInParent<HolsterManager>().weaponInHand;
                    selectedWeapon = hit.transform.gameObject;

                    //lr = selectedWeapon.GetComponentInParent<HolsterManager>().weaponInHandLr;

                    selectedWeapon.SetActive(false);
                    weaponInHand.SetActive(true);

                    pointer.SetActive(false);

                    LaserBegin();
                    isHoldingWeapon = true;
                    GameManager.instance.weaponInHand = weaponInHand;
                    GameManager.instance.isHoldingWeapon = isHoldingWeapon;
                }
            }
        }
        else
            SetLaserColor(Color.white, Color.white);
    }

    void LaserBegin ()
    {
        // Set some positions
        Vector3[] positions = new Vector3[2];
        positions[0] = new Vector3(0f, 0f, 0.0f);
        positions[1] = new Vector3(0.0f, 0.0f, 0.5f);
        lr.positionCount = positions.Length;
        lr.SetPositions(positions);

        SetLaserColor(Color.white, Color.white);
    }

    void SetLaserColor (Color startColor, Color endColor)
    {
        // A simple 2 color gradient with a fixed alpha of 1.0f.
        float alpha = 1.0f;
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(startColor, 0.0f), new GradientColorKey(endColor, .8f) },
            new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(0, 1.0f) }
        );
        lr.colorGradient = gradient;
    }

    void PutAwayWeapon(GameObject currentWeapon)
    {
        currentWeapon.SetActive(false);

        pointer.SetActive(true);
        selectedWeapon.SetActive(true);

        lr = pointerLr;
        LaserBegin();
        isHoldingWeapon = false;
        GameManager.instance.weaponInHand = null;
        GameManager.instance.isHoldingWeapon = isHoldingWeapon;
    }
}
