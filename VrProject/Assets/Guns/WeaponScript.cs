﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class WeaponScript : MonoBehaviour {

    public GameObject weaponObject;                 // Model Of The Object, Should Be Located On The Hand Root
    public Text debugText;
    [System.Serializable]
    public class WeaponVariables
    {
        public LayerMask whatToHitMask;

        public float weaponDamage = 10f;
        public float maxRange = 100f;
    }
    [Space]
    public WeaponVariables weaponVariables;

    [Header("---Weapon Stats---")]
    public ShootMode shootMode;                     // Referance to The Enum
    public enum ShootMode { Auto, Semi, Burst }
    public float timeBetweenShots;                  //The Weapons Fire Rate
    public float reloadTime;                        //Time Taken to Reload

    [Header("---Burst Stats---")]
    public int burstShots = 3;                      //Shots Per Burst
    public float timeBetweenBurst;

    [HideInInspector] public int burstShotsFired;                    //Shots Taken
    [HideInInspector] public bool burstState;                        //Bool for Burst Fire

    [Header("---Bullet Stats---")]
    public Transform firePoint;                     //The Weapons Fire Point
    public ParticleSystem muzzleFlash;              //The Flash Effect of the Gun;
    public GameObject bulletPrefab;

    [HideInInspector] public float shotCounter;                      //Time Counter for Fire Rate

    [Header("---Ammo Stats---")]
    public float currentBullets;                    //Current Amount of Bullets
    [Space]
    public float bulletsPerMag = 30;                // Bullets Per Each Magazine
    public float bulletsLeft = 200;                 // Total Bullets Left

    [Header("---Weapon Bools---")]
    public bool canFire;                            //If the Weapon can Fire
    public bool isReloading;                        //If the Player is Reloading

    public bool usingBurst;                         //If the Weapon Type is Burst
    public bool usingAuto;                          //If the Weapon Type is Auto
    public bool usingSemi;                          //If the Weapon Type is Semi

    private bool shootInput;                        //Bool for the type of Fire

    public TMPro.TextMeshProUGUI text;

    [Header("---Weapon Sounds---")]
    public AudioClip fireSound;
    public AudioClip raloadSound;
    public AudioClip enemyDamage;

    public AudioSource soundHolder;
    public AudioSource soundHolder2;

    private void Start ()
    {
        currentBullets = bulletsPerMag;
    }
	
	private void Update ()
    {
        if (GameManager.instance.isPaused == true || GameManager.instance.isHoldingWeapon == false)
            return;

        Debug.Log("Nice");

        FireType();

        BurstFire();

        WeaponShootMode();
        AmmoCounter();

        text.text = currentBullets + " " + " / " + bulletsLeft;
    }

    public Vector2 GetDirection(Vector2 input)
    {
        Vector2[] directions = new Vector2[]
        {
        Vector2.up,
        Vector2.right,
        Vector2.down,
        Vector2.left
        };

        Vector2 direction = Vector2.zero;
        float max = Mathf.NegativeInfinity;

        foreach (Vector2 vec2 in directions)
        {
            float dot = Vector2.Dot(vec2, input.normalized);

            if (dot > max)
            {
                direction = vec2;
                max = dot;
            }
        }

        return direction;
    }

    private void FireType()
    {
        if (shootInput && canFire)
        {
            if (!usingBurst)
            {
                WeaponFire();
            }
            else
                burstState = true;            
        }

        if (shotCounter >= -1)
            shotCounter -= Time.deltaTime;
    }

    public void RegularFire()
    {
        if (shotCounter <= 0)
        {
            //GetComponentInParent<NetworkAnimator>().SetTrigger("FireWeaponOnce");

            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            soundHolder.PlayOneShot(fireSound);

            shotCounter = timeBetweenShots;
            currentBullets --;            

            if (burstState)
                burstShotsFired++;
        }
    }

    public void WeaponFire()
    {
        if (shotCounter <= 0)
        {
            RaycastHit _hit;
            if (Physics.Raycast(firePoint.position, firePoint.forward, out _hit, weaponVariables.maxRange, weaponVariables.whatToHitMask))
            {
                if (_hit.collider.tag == "Enemies" || _hit.transform.GetComponent<Enemy>())
                {
                    RegularFire();
                    Enemy enemyScript = _hit.transform.GetComponent<Enemy>();

                    Debug.Log("Current weapon damage " + weaponVariables.weaponDamage);
                   // debugText.text = "Current weapon damage " + weaponVariables.weaponDamage;
                    enemyScript.DealDamage(weaponVariables.weaponDamage);


                    GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                    soundHolder.PlayOneShot(fireSound);
                    soundHolder2.PlayOneShot(enemyDamage);

                    //bullet.GetComponent<Bullet>().target = enemyScript.gameObject;
                    //transform.LookAt(target);

                }                
            }

            //muzzleEffect.Play();
            RegularFire();

        }
       
    }


    private void BurstFire()
    {
        if (!usingBurst || !burstState)
            return;

        if (burstShotsFired < burstShots)
            WeaponFire();

        if (burstShotsFired == burstShots || isReloading)
        {
            burstState = false;
            burstShotsFired = 0;
            StartCoroutine(Wait());
        }
    }

    private IEnumerator Wait()
    {
        canFire = false;

        yield return new WaitForSeconds(timeBetweenBurst);

        canFire = true;
    }

    private void WeaponShootMode()
    {
        switch (shootMode)
        {
            case ShootMode.Auto:
                shootInput = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger);
                usingBurst = false;
                usingAuto = true;
                usingSemi = false;
                break;

            case ShootMode.Semi:
                shootInput = Input.GetKeyDown(KeyCode.Q) || OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
                usingBurst = false;
                usingAuto = false;
                usingSemi = true;
                break;

            case ShootMode.Burst:
                shootInput = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
                usingBurst = true;
                usingAuto = false;
                usingSemi = false;
                break;
        }
    }

    private void AmmoCounter()
    {
        if ((GetDirection(OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad)) == Vector2.left && OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad) && currentBullets < bulletsPerMag) || currentBullets <= 0)
        {
            canFire = false;
            StartCoroutine(DoReload());
        }
    }

    private IEnumerator DoReload()
    {
        if (!isReloading)
        {
            //IKHandler IK = GetComponentInParent<IKHandler>();
            //IK.useIK = false;

            isReloading = true;

            //GetComponentInParent<NetworkAnimator>().SetTrigger("ReloadWeapon");           

            soundHolder.PlayOneShot(fireSound);

            yield return new WaitForSeconds(reloadTime);
            //IK.useIK = true;
            //yield return new WaitForSeconds(0.1f);

            Reload();
        }        
    }

    private void Reload()
    {
        if (bulletsLeft <= 0)
            return;

        float bulletsToLoad = bulletsPerMag - currentBullets;
        //                                                         ? = then        : = else
        float bulletsToDeduct = (bulletsLeft >= bulletsToLoad) ? bulletsToLoad : bulletsLeft;    // Amount Needed to Take From Bullets Left       (Smart if Statement)
        // if bulletsLeft >= bulletsToLoad Than use bulletsToLoad, if it is not use bulletsLeft

        bulletsLeft -= bulletsToDeduct;
        currentBullets += bulletsToDeduct;
        isReloading = false;
        canFire = true;
    }
}