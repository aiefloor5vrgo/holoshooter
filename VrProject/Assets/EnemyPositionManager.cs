﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyPositionManager : MonoBehaviour
{
    public Transform player;
    public float behindMaxAngle = 1;
    public float leftMaxAngle = 2;
    public float rightMaxAngle = 2;

    private float angle;

    private float dot;
    private float distance;
    private float sideDirections;

    public bool isOutOfView = false;

    private void Start()
    {
        player = GameManager.instance.playerGameobject.transform;

        //InvokeRepeating("DetectAngle", 1, .2f);
    }

    void Update()
    {
        if (GameManager.instance.isPaused || GameManager.instance.listOfEnemies[0].gameObject != this.gameObject)
            return;

        Vector3 reletiveNormalizedPos = (player.position - transform.position).normalized;
        dot = Vector3.Dot(reletiveNormalizedPos, player.forward);
        angle = Mathf.Acos(dot);

        distance = Vector3.Distance(transform.position, player.position);
        if (distance > 20 || distance < 0)
            return;

        sideDirections = AngleDir(player.forward, (player.position - transform.position), transform.up);

        if (angle < behindMaxAngle)
        {
            isOutOfView = true;
            Debug.DrawLine(transform.position, player.position, Color.red);
            Debug.LogWarning("Enemy Behind Player!");
            GameManager.instance.warningText.text = "Enemy Approaching Behind you!";

            GameManager.instance.rightBar.gameObject.SetActive(false);
            GameManager.instance.leftBar.gameObject.SetActive(false);
            GameManager.instance.bottomBar.gameObject.SetActive(true);

            StartCoroutine(FadeIn(GameManager.instance.warningText.GetComponentInParent<CanvasGroup>()));
        }
        else if (sideDirections == 1 && angle > behindMaxAngle && angle < rightMaxAngle)
        {
            isOutOfView = true;
            Debug.DrawLine(transform.position, player.position, Color.blue);
            Debug.LogWarning("Enemy Left of Player!");
            GameManager.instance.warningText.text = "Enemy Approaching Left!";

            GameManager.instance.rightBar.gameObject.SetActive(false);
            GameManager.instance.leftBar.gameObject.SetActive(true);
            GameManager.instance.bottomBar.gameObject.SetActive(false);

            StartCoroutine(FadeIn(GameManager.instance.warningText.GetComponentInParent<CanvasGroup>()));
        }
        else if (angle > behindMaxAngle && sideDirections == -1 && angle < leftMaxAngle)
        {
            isOutOfView = true;
            Debug.DrawLine(transform.position, player.position, Color.green);
            Debug.LogWarning("Enemy Right of Player!");
            GameManager.instance.warningText.text = "Enemy Approaching Right!";

            GameManager.instance.rightBar.gameObject.SetActive(true);
            GameManager.instance.leftBar.gameObject.SetActive(false);
            GameManager.instance.bottomBar.gameObject.SetActive(false);

            StartCoroutine(FadeIn(GameManager.instance.warningText.GetComponentInParent<CanvasGroup>()));
        }
        else
        {
            isOutOfView = false;
            GameManager.instance.warningText.text = "";
            
            StartCoroutine(FadeOut(GameManager.instance.warningText.GetComponentInParent<CanvasGroup>()));
        }
    }

    IEnumerator FadeIn(CanvasGroup canvasGroup)
    {
        float t = 1f;

        while (t > 0f)
        {
            t += Time.deltaTime;
            float a = GameManager.instance.curve.Evaluate(t);
            canvasGroup.alpha = a;
            yield return 0;
        }
    }

    IEnumerator FadeOut(CanvasGroup canvasGroup)
    {
        float t = 0f;

        while (t < 1f)
        {
            t -= Time.deltaTime;
            float a = GameManager.instance.curve.Evaluate(t);
            canvasGroup.alpha = a;
            yield return 0;
        }

    }

    //returns -1 when to the left, 1 to the right
    public float AngleDir(Vector3 playerForward, Vector3 targetDir, Vector3 up)
    {
        Vector3 crossProduct = Vector3.Cross(playerForward, targetDir);
        float dir = Vector3.Dot(crossProduct, up);

        if (dir > 0.0f)
            return 1.0f;

        else if (dir < 0.0f)        
            return -1.0f;

        else
            return 0;
    }
}
